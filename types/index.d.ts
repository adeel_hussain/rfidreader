// Type definitions for Cordova Rfid Reader
// Definitions by: Microsoft Open Technologies Inc <http://msopentech.com>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// 
// Copyright (c) Microsoft Open Technologies Inc
// Licensed under the MIT license.

interface Test {
    start(
        successCallback: (param: any) => void,
        failureCallback: () => void,
        action: string
    ): void;
    
    stop(
        successCallback: (param: any) => void,
        failureCallback: () => void,
        action: string
    ): void;
}