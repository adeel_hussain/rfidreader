// let argscheck = require('cordova/argscheck');
// let utils = require('cordova/utils');

var channel = require('cordova/channel');
var cordova = require('cordova');

channel.createSticky('onCordovaInfoReady');

channel.waitForInitialization('onCordovaInfoReady');

function RfidReader () {
    this.test = function (successCallback, failureCallback, action) {
        cordova.exec(successCallback, failureCallback, "RfidReader", action, []);
    }

    this.start = function (successCallback, failureCallback) {
        cordova.exec(successCallback, failureCallback, "RfidReader", "start", []);
    }

    this.stop = function (successCallback, failureCallback) {
        cordova.exec(successCallback, failureCallback, "RfidReader", "stop", []);
    }
}

module.exports = new RfidReader();