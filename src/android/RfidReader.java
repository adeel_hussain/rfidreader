package org.apache.cordova.rfidReader;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

public class RfidReader extends CordovaPlugin {
    
    public static Lf125KManager manager = new Lf125KManager();

    public RfidReader() {}

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        System.out.print("Test");
    }

    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equalsIgnoreCase("start")) {
            manager.Start();
            callbackContext.success("Scanner Started!");
            return true;
        } else if (action.equalsIgnoreCase("stop")) {
            callbackContext.success(manager.getRfid());
            return manager.Close();
        } else if (action.equalsIgnoreCase("test")) {
            callbackContext.success("Hey! it works");
            return true;
        }
        callbackContext.error("failed, java");
        return false;
    }
}